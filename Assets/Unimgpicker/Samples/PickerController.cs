﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Afseh.Controllers;

namespace Kakera
{
    public class PickerController : MonoBehaviour
    {
        [SerializeField]
        private Unimgpicker imagePicker;

		public Image selectedImage;

		public SignUpScreenController signUpController;

        void Awake()
        {
            imagePicker.Completed += (string path) =>
            {
				StartCoroutine(LoadImage(path, selectedImage));
            };
        }

        public void OnPressShowPicker()
        {
            imagePicker.Show("Select Image", "unimgpicker", 1024);
        }

        private IEnumerator LoadImage(string path, Image outputImage)
        {
            var url = "file://" + path;
            var www = new WWW(url);
            yield return www;

            var texture = www.texture;
            if (texture == null)
            {
                Debug.LogError("Failed to load texture url:" + url);
            }

			Sprite sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
			outputImage.sprite = sprite;

			signUpController.UploadImage (outputImage);
        }  
    } 
}