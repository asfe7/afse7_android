﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace Afseh.HelperClasses
{
	public class AfsehEditorApplication : Editor {

		[MenuItem(Strings.EditorMenusStrings.AfsehMenu+"/"+Strings.EditorMenusStrings.AfsehModelMenu+"/"+Strings.EditorMenusStrings.AfsehModelMenuStrings.UserAuth)]
		public static void Hello()
		{
			Debug.Log ("Hello This is Afse7 World");
		}

		[MenuItem(Strings.EditorMenusStrings.AfsehMenu+"/"+Strings.EditorMenusStrings.AfsehHelperMenu+"/"+Strings.EditorMenusStrings.AfsehHelperMenuStrings.FixArabicText)]
		public static void FixAllArabicText()
		{
			GameObject.FindObjectOfType<FixArabicText> ().arabicText.text = ArabicSupport.ArabicFixer.Fix (GameObject.FindObjectOfType<FixArabicText> ().arabicText.text.ToString ());
		}

		[MenuItem(Strings.EditorMenusStrings.AfsehMenu+"/"+Strings.EditorMenusStrings.AfsehHelperMenu+"/"+Strings.EditorMenusStrings.AfsehHelperMenuStrings.AssignArabicFixers)]
		public static void AssignArabicFixers()
		{
			Text[] SceneTextHolders = GameObject.FindObjectsOfType<Text> ();
			foreach (Text obj in SceneTextHolders) 
			{
				if (!obj.gameObject.GetComponent<FixArabicText> ()) 
				{
					obj.gameObject.AddComponent<FixArabicText> ();
				}
			}
		}
	}
		
}
