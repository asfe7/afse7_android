﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

namespace Afseh.View 
{
	public class ScreenFader : MonoBehaviour {

		public Image FadeBG;

		private void OnEnable()
		{
			if (System.Object.ReferenceEquals (FadeBG, null))
				FadeBG = this.GetComponent<Image> ();

				ResetFadeEffect ();
				PlayFadeOut ();
		} 
		private void PlayFadeOut()
		{
			FadeBG.DOFade (0f, 1f).OnComplete (()=>{

				this.gameObject.SetActive(false);
			});
		}
		private void ResetFadeEffect()
		{
			FadeBG.color = new Color (FadeBG.color.r,FadeBG.color.g,FadeBG.color.b,1f);
		}
	}
} 
