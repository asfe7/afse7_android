﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Afseh.HelperClasses;
using Afseh.Controllers;
namespace Afseh.View 
{
	public class LoginScreenView : MonoBehaviour {

		public UIPopUpView popUp;

		#region Class Atrributes 
		public InputField emailField, passwordField; 
		#endregion
 
		#region Visible Class Methods	
		public void OnLogin()
		{
			if (!SafetyChecks.IsObjectNull (OnLoginClicked))
				OnLoginClicked (); 
		}
		public void OnGuestLogin()
		{
			SceneManager.LoadScene (SceneName.HomeScreen.ToString (), LoadSceneMode.Single);
		}
		public void OnSignUpButtonClicked()
		{
			SceneManager.LoadScene (SceneName.SignUpScreen.ToString (), LoadSceneMode.Single);
		} 
		public void CreateFeedbackPopUp(string title, string message)
		{
			this.popUp.SetPopUpInfo (title, message);
		}
		public void  OnBackButtonClicked()
		{
			SceneManager.LoadScene (SceneName.WelcomeScreen.ToString (), LoadSceneMode.Single);
		}
		public void ShowPopUp()
		{
			this.popUp.SetPopUpActive (true);
		}
		public void HidePopUp()
		{
			this.popUp.SetPopUpActive (false);
		}
		#endregion

		#region Private Class Methods
		private void Reset()
		{
			FindInspectorReferences ();
		}
		private void Awake()
		{
			FindInspectorReferences ();
		} 
		private void FindInspectorReferences()
		{
			if (SafetyChecks.IsObjectNull (emailField)) 
				emailField = GameObject.Find (("emailField")).GetComponent<InputField> ();
			if (SafetyChecks.IsObjectNull (passwordField)) 
				passwordField = GameObject.Find (("passwordField")).GetComponent<InputField> ();
		}
			
		#endregion

		#region Events Subscription
			public static event CustomEvents.LoginClicked OnLoginClicked; 
		#endregion
	}  		
} 
