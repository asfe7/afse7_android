﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Afseh.View{

public class UIPopUpView : MonoBehaviour{

	public Text popUpTitle, popUpMessage;
 
	public void SetPopUpInfo(string titleText = "Title",string messageText = "Message")
	{
		this.popUpTitle.text = titleText;
		this.popUpMessage.text = messageText;
	} 

	private void ShowPopUp()
	{
		this.gameObject.SetActive (true);
	}

	private void HidePopUp()
	{
		this.gameObject.SetActive (false);
	}

	public void SetPopUpActive(bool state)
	{
		if (state)
			this.ShowPopUp ();
		else
			this.HidePopUp ();
	}

	public void OnDoneClicked()
	{
		this.SetPopUpActive (false);
	} 
}

}
