﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UPersian.Utils;

public class PostData : MonoBehaviour {

	private string userId = "";
	private string postContent = "";
	private string userName = "";

	public Text userNameText;
	public Text postContentText;


	public string UserId {
		get {
			return userId;
		}

		set
		{
			if (value != null) 
			{
				userId = value;
				userNameText.text = userId;
			}

		}
	}

	public string PostContent {
		get {
			return postContent;
		}

		set
		{
			if (value != null)
			{
				postContent = value;
				postContentText.text = postContent;
			}
		}
	}

	public string UserName {
		get {
			return userName;
		}

		set
		{
			if (value != null) 
			{
				userName = value;
				userNameText.text = userName;
			}

		}
	} 

	public void SetPostData(string userId , string postContent)
	{
		this.UserId = userId;
		this.PostContent = postContent;
	}
}
