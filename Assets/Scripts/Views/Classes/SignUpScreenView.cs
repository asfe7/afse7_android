﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using Afseh.HelperClasses;

namespace Afseh.View
{
	public class SignUpScreenView : MonoBehaviour {

		public InputField fullNameField, emailField, passwordField, birthdayField; 
		public UIPopUpView PopUp;

		private Text fullNameText, emailText, passwordText, birthdayText;

		private void Reset()
		{
			FindReferencesOnReset ();
			CheckReferences ();
		}
		private void Awake ()
		{
			CheckReferences ();
		}	
		private void CheckReferences()
		{
			if (SafetyChecks.IsObjectNull (emailText) || SafetyChecks.IsObjectNull (passwordText) ||
				SafetyChecks.IsObjectNull (fullNameText) || SafetyChecks.IsObjectNull (birthdayText))
				AssignReferences ();
		}
		private void FindReferencesOnReset()
		{
			emailField    = GameObject.Find (("emailField"   )).GetComponent<InputField> ();
			fullNameField = GameObject.Find (("fullNameField")).GetComponent<InputField> ();
			passwordField = GameObject.Find (("passwordField")).GetComponent<InputField> ();
			birthdayField = GameObject.Find (("birthdayField")).GetComponent<InputField> ();
		}	 
		private void AssignReferences()
		{
			if (!SafetyChecks.IsObjectNull (emailField)) 
				emailText = emailField.textComponent;
			
			if (!SafetyChecks.IsObjectNull (passwordField))
				passwordText = passwordField.textComponent;

			if (!SafetyChecks.IsObjectNull (fullNameField))
				fullNameText = fullNameField.textComponent;

			if (!SafetyChecks.IsObjectNull (birthdayField))
				birthdayText = birthdayField.textComponent;
		} 
	
		public void OnSignUp()
		{

			if (!SafetyChecks.IsObjectNull (OnSignUpClicked)) 
				OnSignUpClicked (); 	
		}  
		public void OnBackButtonClicked()
		{
			SceneManager.LoadScene (SceneName.WelcomeScreen.ToString(),LoadSceneMode.Single);
		}
		public void CreateFeedbackPopUp(string title, string message)
		{
			this.PopUp.SetPopUpInfo (title, message);
		}
		public void ShowPopUp()
		{
			this.PopUp.SetPopUpActive (true);
		}
			
		#region Events Subscription
		public static event CustomEvents.SignUpClicked OnSignUpClicked; 
		#endregion
			
	}    
		
}
