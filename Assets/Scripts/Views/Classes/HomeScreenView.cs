﻿using UnityEngine;
using UnityEngine.UI;
using Afseh.HelperClasses;
using UnityEngine.SceneManagement;
using UPersian.Components;

namespace Afseh.View 
{
	public class HomeScreenView : MonoBehaviour {

		public UIPopUpView popUp;

		public GameObject newPostHolder;

		public GameObject PostPrefab,PostsHolder;

		public RtlText rtlText;

		public string postContent;

		public void OpenMyProfile()
		{
			SceneManager.LoadScene (SceneName.ProfileScreen.ToString(), LoadSceneMode.Single);
		}  

		public void OpenHomeScreen()
		{
			SceneManager.LoadScene (SceneName.HomeScreen.ToString(), LoadSceneMode.Single);
		}

		public void CreateFeedbackPopUp(string title, string message)
		{
			this.popUp.SetPopUpInfo (title, message);
		}

		public void ShowPopUp()
		{
			this.popUp.SetPopUpActive (true);
		}

		public void HidePopUp()
		{
			this.popUp.SetPopUpActive (false);
		}

		public void HidePostHolder()
		{
			newPostHolder.SetActive (false);
		}

		public void OnPostButtonClicked()
		{
			postContent = rtlText.BaseText.ToString ().Trim (); // Get the base non formated text out of there
			//Debug.Log ("rtlText "+postContent);
			if (!string.IsNullOrEmpty (postContent))
				if (OnPostClicked != null)
					OnPostClicked (postContent,"ala@afseh.com");
		} 

		public void CreateNewPost(string postContent, string userId)
		{
			GameObject postObject = Instantiate(PostPrefab,PostsHolder.transform,false) as GameObject;
			postObject.GetComponent<PostData> ().SetPostData (userId, postContent);
		}

		#region Events Subscription
		public static event CustomEvents.PostContentClicked OnPostClicked;
		#endregion
	}
}
 