﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Afseh.HelperClasses;

namespace Afseh.Model {

	[CreateAssetMenu(menuName = Strings.EditorMenusStrings.AfsehMenu+"/"+
		Strings.EditorMenusStrings.AfsehModelMenu+"/"+
		Strings.EditorMenusStrings.AfsehModelMenuStrings.UserAuth,
		fileName = Strings.EditorMenusStrings.AfsehModelMenuStrings.UserAuthDefaultFileName+".asset")]

	public class UserAuthenticationSettings : ScriptableObject {

			[Header("Authentication Settings")]

			[Header("FullName Requirements")]
			[Tooltip("name Length")]
			[Range(1 , 20)]

			public int fullNameLength = 5;

			public bool canHasUpperCaseLetter = true;
			public bool canHasLowerCaseLetter = true;
			public bool canHasSpecialCharacter = false;
			public bool canHasNumber = false;
		
			[Header("Password Requirements")]
			[Tooltip("password Length")]
			[Range(6 , 20)]
			public int passwordLength = 6;

			public bool mustHasUpperCaseLetter = false;
			public bool mustHasLowerCaseLetter = false;
			public bool mustHasSpecialCharacter = false;
			public bool mustHasNumber = true; 


	}
}
	
