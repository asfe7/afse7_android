﻿namespace Afseh.HelperClasses
{
	public static class Strings {

		public static class EditorMenusStrings
		{
			public const string AfsehMenu = "Afse7";
			public const string AfsehControllerMenu = "Controller";
			public const string AfsehModelMenu = "Model";
			public const string AfsehViewMenu = "View";
			public const string AfsehHelperMenu = "Helper";

			public static class AfsehModelMenuStrings
			{
				public const string UserAuth = "Create User Auth Settings File";
				public const string UserAuthDefaultFileName = "UserAuthSettings";
			}

			public static class AfsehHelperMenuStrings
			{
				public const string AssignArabicFixers = "Assign Arabic Fixers";
				public const string FixArabicText = "Fix All Arabic Text Instances";
			}


		}
	}
		
}
