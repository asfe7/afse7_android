﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Afseh.HelperClasses;

[RequireComponent(typeof(Text))]
public class FixArabicText : MonoBehaviour {

	public Text arabicText;
	// Use this for initialization
	void Reset () 
	{
		if (SafetyChecks.IsObjectNull (arabicText)) 
		{ 
			arabicText = this.GetComponent<Text> (); 
		}
	}

}
