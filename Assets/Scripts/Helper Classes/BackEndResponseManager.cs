﻿using System.Collections.Generic;
using UnityEngine;
using Afseh.HelperClasses;

namespace Afseh.HelperClasses
{
	[System.Serializable]
	public class BackEndResponseManager 
	{
		public string status;
		public string message;
		public string developerMessage;
		public List<Data> data;

		[System.Serializable]
		public class Data
		{
			public string content;
			public string userId;
		}

		public static BackEndResponseManager CreateFromJson(string jsonString)
		{
			return JsonUtility.FromJson<BackEndResponseManager>(jsonString);
		} 
	}

	[System.Serializable]
	public class BackEndAddPostResponseManager 
	{
		public string status;
		public string message;
		public string developerMessage;
		public Data data;

		[System.Serializable]
		public class Data
		{
			public string content;
			public string userId;
		}

		public static BackEndAddPostResponseManager CreateFromJson(string jsonString)
		{
			return JsonUtility.FromJson<BackEndAddPostResponseManager>(jsonString);
		} 
	}


	[System.Serializable]
	public class ImageUploadResponse
	{
		public string status;
		public string message;
		public string developerMessage;
		public Data data;

		[System.Serializable]
		public class Data
		{
			public string url;
		}

		public static ImageUploadResponse CreateFromJson(string jsonString)
		{
			return JsonUtility.FromJson<ImageUploadResponse>(jsonString);
		}  
	}
		
} 
