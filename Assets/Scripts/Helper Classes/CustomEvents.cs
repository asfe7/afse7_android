﻿namespace Afseh.HelperClasses
{
	public static class CustomEvents {

		#region Delegates Creation

		#region User Authentication Delegates
		public delegate void LoginClicked(); 
		public delegate void SignUpClicked(); 
		public delegate void EditClicked(); 
		public delegate void PostContentClicked(string postContent, string username);
		#endregion
		 
		#endregion

	}
}
