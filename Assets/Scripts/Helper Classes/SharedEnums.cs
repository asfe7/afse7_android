﻿namespace Afseh.HelperClasses
{
	public enum SceneName {

		SplashScreen,
		LoginScreen,
		WelcomeScreen,
		SignUpScreen,
		HomeScreen,
		ProfileScreen
	}

	public enum EntryType
	{
		standardText, emailAddress, password, fullName, birthday
	}
}
	 
