﻿namespace Afseh.HelperClasses
{
	public class SafetyChecks {

		public static bool IsObjectNull(object objectRef)
		{
			if (System.Object.ReferenceEquals (objectRef, null))
				return true;
			return false;
		}

		public static bool IsStringNullOrEmpty(string str)
		{
			if (string.IsNullOrEmpty (str))
				return true;
			return false;
		}
	}  
}
