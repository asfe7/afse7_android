﻿using UnityEngine;
using System.Text.RegularExpressions;

namespace Afseh.HelperClasses
{
	public static class InputFieldValidation {

		public static string validationMessage = "Please Fill In All The Fields";

		#region Password Rules
		private static int passwordMinLength = 6;
		private static int passwordMaxLength = 20;
		private static bool passwordMeetsLengthRequirements = false;
		#endregion

		#region FullName Rules
		private static int fullNameMinLength = 6;
		private static int fullNameMaxLength = 20;
		private static bool fullNameMeetsLengthRequirements = false;
		#endregion

		#region Common Input Rules
		private static bool hasUpperCaseLetter 		= true;
		private static bool hasLowerCaseLetter 		= true;
		private static bool hasNumber	    		= true;
		private static bool hasSpecialCharacter     = true;
		#endregion

		public static bool IsNotEmpty(string entry)
		{
			if (!SafetyChecks.IsStringNullOrEmpty (entry))
				return true;
			else
			{  
				return false;
			}
		}

		public static bool IsNotEmpty(string []entries)
		{
			bool isNotEmpty = true;
			for (int x = 0; x < entries.Length; x++) 
			{
				if (SafetyChecks.IsStringNullOrEmpty (entries [x]))
					isNotEmpty = false;
			}
				
			return isNotEmpty;
		}

		public static bool ValidateInput(string entryString, EntryType entryType)
		{
			bool isValid = false;

			switch (entryType) 
			{

			case EntryType.emailAddress:

				if (ValidateEmailAddress (entryString)) 
					isValid = true;
			
				else
				{
					isValid = false;

				}

				break;  

			case EntryType.password:

				if (ValidatePassword(entryString))
					isValid = true;

				else 
				{
					isValid = false;
				} 

				break; 

			case EntryType.fullName:
				
				if (ValidateFullName (entryString)) 
				{
					isValid = true;
				}


				else 
				{
					isValid = false;
					//validationMessage = "Full name is Invalid";
				} 


				break;

			case EntryType.birthday:
				
				if (ValidateBirthday(entryString))
					isValid = true;

				else 
				{
					isValid = false;
					validationMessage = "Birthday is Invalid";
				} 

				break;
			} 

			return isValid;
		}

		public static string[] DetectFullNameParts(string entryString)
		{
			string fullName = entryString.Trim();

			string firstName ="";
			string lastName = "";

			char[] fullNameChars = fullName.ToCharArray ();
			bool spaceFound = false;

			for (int x = 0; x < fullNameChars.Length; x++) 
			{
				if (spaceFound == false) 
				{
					firstName += fullNameChars [x];
				}

				if (spaceFound) 
				{
					lastName+= fullNameChars [x];
				}

				if (fullNameChars [x] == ' ') 
				{
					spaceFound = true;
				}
			}

			string[] nameParts = new string[]{ firstName, lastName};

			return nameParts; 
		}		 

		#region Validation Types
		private static bool ValidateEmailAddress(string strIn)
		{
			validationMessage = "InValid Email Format please try again";

			Regex regex = new Regex(@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
				@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$");

			Match match = regex.Match(strIn);

			if (match.Success)
				return true;
				return false;	
		} 
		private static bool ValidatePassword(string password)
		{
			if (SafetyChecks.IsStringNullOrEmpty (password))
				return false;

			 passwordMeetsLengthRequirements = password.Length >= passwordMinLength && password.Length <= passwordMaxLength;

			if ( passwordMeetsLengthRequirements )
			{
				foreach (char c in password )
				{
					if      ( char.IsUpper (c) ) hasUpperCaseLetter  = true ;
					else if ( char.IsLower (c) ) hasLowerCaseLetter  = true ;
					else if ( char.IsNumber(c) ) hasNumber     		 = true ;
					else if ( char.IsSymbol(c) ) hasSpecialCharacter = true ;
				}
			}

			bool isValid = passwordMeetsLengthRequirements
				&& hasUpperCaseLetter
				&& hasLowerCaseLetter
				&& hasNumber
				&& hasSpecialCharacter;

			if (!isValid) 
			{
				validationMessage = "Invalid Password format";

				if(!passwordMeetsLengthRequirements)
					validationMessage = "Password Length Must be at least 6 characters";
				else if(!hasUpperCaseLetter)
					validationMessage = "Password Must has atleast one Upper Case letter";
				else if(!hasLowerCaseLetter)
					validationMessage = "Password Must has atleast one Lower Case letter";
				else if (!hasNumber)
					validationMessage = "Password Must has atleast one Number";
				else if (!hasSpecialCharacter)
					validationMessage = "Password Must has atleast one Special Character";
			}
			
			return isValid;
		}
		private static bool ValidateFullName(string fullName)
		{
			bool isValid = true;

			fullNameMeetsLengthRequirements = fullName.Length >= fullNameMinLength && fullName.Length <= fullNameMaxLength;

			isValid = fullNameMeetsLengthRequirements;

//			if (fullNameMeetsLengthRequirements) 
//			{
//				char[] chars = fullName.ToCharArray ();
//
//				for (int x = 0; x < chars.Length; x++) 
//				{
//					if (char.IsNumber (chars [x])) 
//					{
//						hasNumber = true;
//						Debug.Log ("number "+chars[x]);
//					}
//
//					else if (char.IsSymbol (chars [x]))
//						hasSpecialCharacter = true;
//				}
//
//				if (hasNumber || hasSpecialCharacter)
//					isValid = false;
//			}

			if (!isValid) 
			{
				if (!fullNameMeetsLengthRequirements)
					validationMessage = "Full name must be atleast 6 characters and less than 20 character!";
				else if (hasNumber)
					validationMessage = "Full name can't has numbers just letters!";
				else if (hasSpecialCharacter)
					validationMessage = "Full name can't has special characters just letters!";
			}
			 
			return isValid;
		}
		private static bool ValidateBirthday(string birthday)
		{
			if (birthday.Length >= 0)
				return true;
			return false;
		}
		#endregion
	}  
}
 