﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FixArabicText))]
public class FixArabicTextEditor : Editor {

	private FixArabicText fixArabicText;

	void OnEnable()
	{
		fixArabicText = (FixArabicText)target;
	}
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		if (GUILayout.Button ("Fix Arabic Text")) 
		{
			fixArabicText.arabicText.text = ArabicSupport.ArabicFixer.Fix (fixArabicText.arabicText.text.ToString ());
		}
	}
}
