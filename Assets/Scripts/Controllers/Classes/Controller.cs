﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Afseh.HelperClasses;

namespace Afseh.Controllers 
{
	public class Controller : MonoBehaviour, IController {
	 
		public SceneName sceneName;

		#region IController implementation

		public void LoadScene ()
		{
			SceneManager.LoadScene (sceneName.ToString (), LoadSceneMode.Single);
		} 

		public void ReloadScene()
		{
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex, LoadSceneMode.Single);
		}

		#endregion
	} 
}
