﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Afseh.HelperClasses;
using Afseh.View;
using Afseh.Model;
using UnityEngine.Analytics;
using System.Text;

namespace Afseh.Controllers
{
	public class HomeScreenController : Controller {

		public HomeScreenView homeScreenView;   
		public string addPostServiceURL = "http://afseh.com:8080/posts/save";
		public string getPostsServiceURL = "http://afseh.com:8080/posts";
		 
		private void OnEnable()
		{
			HomeScreenView.OnPostClicked += OnPostClicked;
			GetPosts ();
		}
		 
		private void OnDisable()
		{
			HomeScreenView.OnPostClicked -= OnPostClicked;
		}

		private void OnPostClicked(string content, string username)
		{
			//Debug.Log ("Post Content "+content + " username "+username);
			PrepareDataForAsync (content, username);
		}
		 
		private void GetPosts()
		{
			Dictionary<string,string> headers = new Dictionary<string, string>();

			headers.Add("Content-Type", "application/json");

			headers.Add("Accept-Language ", "ar");

			WWW api = new WWW(getPostsServiceURL,null,headers);
			StartCoroutine(GetPostAsync(api));
		} 

		private void LoadNextScene()
		{ 
			LoadScene ();
		} 

		private void ReloadCurrentScene()
		{
			ReloadScene ();
		}
			
		private void PrepareDataForAsync(string postContent, string username)
		{
			Post post = new Post ();

			post.userEmail = username;
			post.content = postContent;

			Debug.Log ("User Email "+post.userEmail);

			string requestJson = JsonUtility.ToJson (post);

			Dictionary<string,string> headers = new Dictionary<string, string>();

			headers.Add("Content-Type", "application/json");

			headers.Add("Accept-Language ", "en");

			byte[] pData = System.Text.Encoding.UTF8.GetBytes(requestJson);

			//byte[] pData = System.Text.Encoding.ASCII.GetBytes(requestJson.ToCharArray());
			 
			WWW api = new WWW(addPostServiceURL, pData,headers);

			StartCoroutine(PostAsync(api));
		}

		private IEnumerator PostAsync(WWW www)
		{
			Debug.Log ("PostAsync POST, Waiting Response");

			yield return www;

			string requestResult = www.text;
			 
			//if(requestResult != null)
				//Debug.Log ("Result "+requestResult);

			BackEndAddPostResponseManager response = BackEndAddPostResponseManager.CreateFromJson (requestResult);
			//Debug.Log ("Status Number "+response.status);
			//Debug.Log ("Message "+response.message);

			if (response.status != "200") {
				//homeScreenView.CreateFeedbackPopUp ("Post Fail", ArabicSupport.ArabicFixer.Fix (response.message));
				//homeScreenView.ShowPopUp ();
			}
			else if(response.status == "200")
			{
				//Debug.Log ("Message "+response.message);
				homeScreenView.HidePostHolder ();
				ReloadCurrentScene ();
			}  

			yield break;
		}

		private IEnumerator GetPostAsync(WWW www)
		{
			Debug.Log ("GetPostAsync , Waiting Response");

			yield return www;

			string requestResult = www.text;

			//if(requestResult != null)
				//Debug.Log ("Result "+requestResult);

			BackEndResponseManager response = BackEndResponseManager.CreateFromJson (requestResult);

			//Debug.Log ("Status Number "+response.status);
			//Debug.Log ("Message "+response.message);

			if (response.status != "200") {
				//homeScreenView.CreateFeedbackPopUp ("Post Fail", ArabicSupport.ArabicFixer.Fix (response.message));
				//homeScreenView.ShowPopUp ();
				Debug.Log("Error : "+response.message);
			} 
			else if(response.status == "200")
			{
				CreatePostPrefab (response);
			}

			yield break;
		}

		private void CreatePostPrefab(BackEndResponseManager response)
		{
//			if (response.data.Count > 0) 
//			{
//				for (int x = response.data.Count - 1; x >= ((response.data.Count/2) + (response.data.Count/3)); x--) 
//				{
//					homeScreenView.CreateNewPost (response.data [x].content, response.data [x].userId);
//				}     
//			}

			if (response.data.Count > 0) 
			{
				for (int x = response.data.Count - 1; x >= 0; x--) 
				{ 
					homeScreenView.CreateNewPost (response.data [x].content, response.data [x].userId);
				}     
			}
		}
			
		public void LogoutClicked()
		{
			sceneName = SceneName.WelcomeScreen;
			LoadNextScene ();
		}

		public void OnProfileClicked()
		{
			sceneName = SceneName.ProfileScreen;
			LoadNextScene ();
		}
			
	}



	[System.Serializable]
	public class Post
	{
		public string content;
		public string userEmail;
	}
}