﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Afseh.HelperClasses;
using Afseh.View;
using Afseh.Model;

namespace Afseh.Controllers
{
	public class WelcomeScreenController : Controller {

		private void LoadNextScene()
		{
			LoadScene ();
		}

		public void OnLoginClicked()
		{
			sceneName = SceneName.LoginScreen;
			LoadNextScene ();
		}

		public void OnSignUpClicked()
		{
			sceneName = SceneName.SignUpScreen;
			LoadNextScene ();
		}

		public void OnFBLoginClicked()
		{
			//TODO
		}

		public void OnGuestLoginClicked()
		{
			sceneName = SceneName.HomeScreen;
			LoadNextScene ();
		}
	}
}

