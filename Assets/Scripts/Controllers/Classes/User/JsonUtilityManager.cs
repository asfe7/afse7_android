﻿
namespace Afseh.Controllers
{
public class JsonUtilityManager  {

	[System.Serializable]
	public class User
	{
		public string firstName;
		public string lastName;
		public string birthDate;
		public string gender;
		public Account account;

		[System.Serializable]
		public class Account
		{
			public string email;
			public string username;
			public string password;
			public string imageUrl;
		}   

		public User()
		{

		}

		public User(string firstName,string lastName,string birthDay,string gender, Account account)
		{
			this.firstName = firstName;
			this.lastName = lastName;
			this.birthDate = birthDay;
			this.gender = gender;
			this.account = account;
		}
				

	}
}


//{"firstName":"Unity","lastName":"Engine","birthDate":"1993-11-15","gender":"MALE","account":{"email":"ali@email.com","password":"123456789"}}";	
}
