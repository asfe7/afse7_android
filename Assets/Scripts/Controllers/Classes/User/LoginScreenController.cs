﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Afseh.HelperClasses;
using Afseh.View;
using Afseh.Model;

namespace Afseh.Controllers
{
	public class LoginScreenController : Controller {

		public LoginScreenView loginView;
		public string loginServiceUrl;
		public string emailVerificationSeviceUrl = "http://afseh.com:9998/user/sendverificationcode";
		public string resetPasswordSeviceUrl = "http://afseh.com:9998/user/verifycodeandrestpassword"; 

		private void OnEnable()
		{
			LoginScreenView.OnLoginClicked += this.OnLoginClicked;
		}

		private void OnDisable()
		{
			LoginScreenView.OnLoginClicked -= this.OnLoginClicked; 
		}

		private void OnLoginClicked ()
		{

			if (ValidateInputs (loginView.emailField.text.ToString (), loginView.passwordField.text.ToString ())) 
			{
				PrepareDataForLogin (loginView.emailField.text.ToString (), loginView.passwordField.text.ToString ());
			}
			else
			{
				loginView.CreateFeedbackPopUp ("Login Fail", InputFieldValidation.validationMessage);
				loginView.ShowPopUp ();
			} 
		}

		private void LoadNextScene()
		{
			LoadScene ();
		} 

		private bool ValidateInputs(string email,string password)
		{ 
			bool isValid = false;

			if (InputFieldValidation.IsNotEmpty(new string[]{email,password})) 
			{
				if (InputFieldValidation.ValidateInput (email, EntryType.emailAddress)) 
				{
					if (InputFieldValidation.ValidateInput (password, EntryType.password)) 
					{
						isValid = true;
					} 
				} 
			} 

			return isValid;
		}

		private void PrepareDataForLogin(string username, string password)
		{
			userLogin account = new userLogin ();

			account.email = username;
			account.password = password;

			//string userJson = JsonUtility.ToJson (account);

			string manualJson = "{\"email\":\""+ username +"\",\"password\":\""+ password +"\"}";

			Debug.Log ("userJson "+manualJson);
			 
			Dictionary<string,string> headers = new Dictionary<string, string>();

			headers.Add("Content-Type", "application/json");

			headers.Add("Accept-Language ", "en");

			byte[] pData = System.Text.Encoding.ASCII.GetBytes(manualJson.ToCharArray());

			WWW api = new WWW(loginServiceUrl, pData, headers);

			StartCoroutine(LoginAsync(api));
		}

		private IEnumerator LoginAsync(WWW www)
		{
			Debug.Log ("LoginAsync POST, Waiting Response");

			yield return www;

			string requestResult = www.text;

			if(requestResult != null)
				Debug.Log ("Result "+requestResult);

			BackEndResponseManager response = BackEndResponseManager.CreateFromJson (requestResult);
			Debug.Log ("Status Number "+response.status);
			Debug.Log ("Message "+response.message);

			if (response.status != "200") {
				loginView.CreateFeedbackPopUp ("Login Fail", ArabicSupport.ArabicFixer.Fix (response.message));
				loginView.ShowPopUp ();
			}
			else if(response.status == "200")
			{
				Debug.Log ("Message "+response.message);
				this.LoadNextScene ();
			}

			yield break;
		}

		public void SendEmailVerification(InputField userEmail)
		{
			Dictionary<string,string> headers = new Dictionary<string, string>();

			headers.Add("Content-Type", "application/json");

			//headers.Add("Accept-Language ", "en");

			string recipient = "{\"recipient\":" + "\""+userEmail.text+"\"}";

			Debug.Log ("recipient "+recipient);

			byte[] pData = System.Text.Encoding.ASCII.GetBytes(recipient.ToCharArray());

			WWW api = new WWW(emailVerificationSeviceUrl, pData, headers);

			StartCoroutine (VerifyEmail(api));
		}

		private IEnumerator VerifyEmail(WWW www)
		{
			yield return www;

			string requestResult = www.text;
			Debug.Log ("res "+www.text);
			Debug.Log ("res "+www.error);
			if(requestResult != null)
				Debug.Log ("Result "+requestResult);
		} 

	}

	[System.Serializable]
	public class userLogin
	{
		public string email;
		public string password;
	}
}
 