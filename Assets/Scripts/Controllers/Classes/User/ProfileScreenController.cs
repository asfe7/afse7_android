﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Afseh.HelperClasses;
using Afseh.View;
using Afseh.Model;
using System.Text; 

namespace Afseh.Controllers
{
	public class ProfileScreenController : Controller {

		public UIPopUpView PopUp;
		public string profileImageUrl = "";

		public ProfileScreenView profileScreenView;

		public string followersServiceUrl;
		public string editProfileServiceUrl;
		public string imageUploadServiceUrl;

		public void UploadImage(Image imageSprite)
		{
			Texture2D texture2D = imageSprite.sprite.texture;
			byte[] imageBytes = texture2D.EncodeToPNG ();

			Debug.Log ("I will upload the image for you (; "+imageBytes.Length);

			Dictionary<string,string> headers = new Dictionary<string, string>();

			headers.Add("Content-Type", "application/json");

			headers.Add("Accept-Language", "en");

			byte[] pData = imageBytes;

			WWWForm postForm = new WWWForm();

			postForm.AddBinaryData("image",pData);

			StartCoroutine (ImageUploadAsync (imageUploadServiceUrl,postForm));

		}  

		private void OnEnable()
		{

			Dictionary<string,string> headers = new Dictionary<string, string>();

			headers.Add("Content-Type", "application/json");

			headers.Add("Accept-Language", "en");

			string finalUrl = followersServiceUrl + "ala@ala.com" + "/" + "abed@abed.com";

			WWW api = new WWW(finalUrl);

			StartCoroutine (GetFriendShipStatus (api));

		}

		private void OnDisable()
		{
			//profileScreenView.OnEditClicked -= this.OnEditClicked; 
		}

		private void OnEditClicked()
		{
			if (this.ValidateInputs (profileScreenView.fullNameField.text.ToString (), profileScreenView.emailField.text.ToString (),
				profileScreenView.passwordField.text.ToString (), profileScreenView.birthdayField.text.ToString ()))
			{
				//PrepareDataForApplyEdits (profileScreenView.fullNameField.text.ToString(),profileScreenView.emailField.text.ToString(),
				//	profileScreenView.passwordField.text.ToString(),profileScreenView.birthdayField.text.ToString());
			}   
			else
			{
				if (!SafetyChecks.IsObjectNull (profileScreenView)) 
				{
					profileScreenView.CreateFeedbackPopUp("SignUp Fail", InputFieldValidation.validationMessage);
					profileScreenView.ShowPopUp ();
				}
			} 
		}

		private void LoadNextScene()
		{
			LoadScene ();
		} 

		private bool ValidateInputs(string fullName, string email,string password, string birthday)
		{ 
			bool isValid = false;

			if (InputFieldValidation.IsNotEmpty(new string[]{fullName,email,password,birthday})) 
			{
				if (InputFieldValidation.ValidateInput (fullName, EntryType.fullName)) 
				{
					if (InputFieldValidation.ValidateInput (email, EntryType.emailAddress)) 
					{
						if (InputFieldValidation.ValidateInput (password, EntryType.password)) 
						{
							if (InputFieldValidation.ValidateInput (birthday, EntryType.birthday)) 
							{
								isValid = true;
							} 
						}
					} 
				} 
			} 

			return isValid;
		}

		private void PrepareDataForApplyEdits(string fullName,string email,string password,string birthday,string gender)
		{
			Debug.Log ("Preparing for Edit Async");

			JsonUtilityManager.User user = new JsonUtilityManager.User();
			JsonUtilityManager.User.Account account = new JsonUtilityManager.User.Account ();

			account.email = email;
			account.password = password;
			account.imageUrl = profileImageUrl;

			string[] fullNameParts = InputFieldValidation.DetectFullNameParts (fullName);
			string firstName = fullNameParts [0];
			string lastName = fullNameParts [1];

			user = new JsonUtilityManager.User(firstName,lastName,birthday,gender,account);
			string userJson = JsonUtility.ToJson (user);
			Debug.Log ("User "+userJson);
			//string ourPostData = "{\"firstName\":\"Unity\",\"lastName\":\"Engine\",\"birthDate\":\"1993-11-15\",\"gender\":\"MALE\",\"account\":{\"email\":\"ali@email.com\",\"password\":\"123456789\"}}";

			Dictionary<string,string> headers = new Dictionary<string, string>();

			headers.Add("Content-Type", "application/json");

			headers.Add("Accept-Language", "en");

			byte[] pData = System.Text.Encoding.ASCII.GetBytes(userJson.ToCharArray());

			WWW api = new WWW(editProfileServiceUrl, pData, headers);

			StartCoroutine(SignUpAsync(api));
		}  

		private IEnumerator SignUpAsync(WWW www)
		{
			Debug.Log ("SignUpAsync POST, Waiting Response");

			yield return www;

			string requestResult = www.text;

			if(requestResult != null)
				Debug.Log ("Result "+requestResult);

			BackEndResponseManager response = BackEndResponseManager.CreateFromJson (requestResult);
			//Debug.Log ("Status Number "+response.status);
			//Debug.Log ("Message "+response.message);

			if (response.status != "200") {
				profileScreenView.CreateFeedbackPopUp ("SignUp Fail", ArabicSupport.ArabicFixer.Fix (response.message));
				profileScreenView.ShowPopUp ();
			}
			else if(response.status == "200")
			{
				this.LoadNextScene ();
			}

			yield break;

		}

		private IEnumerator GetFriendShipStatus(WWW www)
		{
			Debug.Log ("SignUpAsync POST, Waiting Response");

			yield return www;

			string requestResult = www.text;

			if(requestResult != null)
				Debug.Log ("Result "+requestResult);

			//BackEndResponseManager response = BackEndResponseManager.CreateFromJson (requestResult);
			//Debug.Log ("Status Number "+response.status);
			//Debug.Log ("Message "+response.message);

//			if (response.status != "200") {
//				profileScreenView.CreateFeedbackPopUp ("SignUp Fail", ArabicSupport.ArabicFixer.Fix (response.message));
//				profileScreenView.ShowPopUp ();
//			}
//			else if(response.status == "200")
//			{
//				this.LoadNextScene ();
//			}
			 
			yield break;

		}

		private void UpdateFriendShipStatus()
		{
			string userJson = "";

			FriendShipManager followStatus = new FriendShipManager ();

			followStatus.followerAccount = "";
			followStatus.followedAccount = "";

			Dictionary<string,string> headers = new Dictionary<string, string>();

			headers.Add("Content-Type", "application/json");

			headers.Add("Accept-Language", "en");

			byte[] pData = System.Text.Encoding.ASCII.GetBytes(userJson.ToCharArray());

			WWW api = new WWW(editProfileServiceUrl, pData, headers);

			StartCoroutine(SignUpAsync(api));
		}

		private IEnumerator ImageUploadAsync(string uploadURL, WWWForm postForm)
		{
			Debug.Log ("ImageUploadAsync POST, Waiting Response");

			WWW upload = new WWW(uploadURL,postForm);        
			yield return upload;
			//if (upload.error == null)
			Debug.Log("upload done :" + upload.text);
			//else
			//Debug.Log("Error during upload: " + upload.error);

			ImageUploadResponse imageUploadResponse = ImageUploadResponse.CreateFromJson (upload.text);

			//Debug.Log ("Image Url "+imageUploadResponse.data.url);

			if(!string.IsNullOrEmpty(imageUploadResponse.data.url))
				profileImageUrl = imageUploadResponse.data.url;
		} 

	}

}

[System.Serializable]
public class FriendShipManager
{
	public string followerAccount;
	public string followedAccount;
	public enum FollowStatus
	{
		ACCEPTED,NO_FRIENDSHIP
	}
	public FollowStatus status;
}


//			{
//				"followerAccount":"abed@gmail.com",
//				"followedAccount":"alaa@gmail.com",
//				"status":"ACCEPTED"
//			}