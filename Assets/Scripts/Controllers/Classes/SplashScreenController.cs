﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Afseh.HelperClasses;
using DG.Tweening;

namespace Afseh.Controllers
{
	public class SplashScreenController : MonoBehaviour {

		#region Inspector Refs
		public SceneName loadScene;
		public RectTransform appTitle,BGFade;
		[Range(0,5)]
		public int loadDelay = 0;
		#endregion

		private void Start()
		{
			PlayIntroAnimation ();
		}
			
		private void PlayIntroAnimation()
		{
			BGAnim ();
			Invoke ("LogoAnim",0.6f);
		}
		private void BGAnim()
		{
			BGFade.GetComponent<Image> ().color = new Color (BGFade.GetComponent<Image> ().color.r, BGFade.GetComponent<Image> ().color.g, BGFade.GetComponent<Image> ().color.b, 0f);
			BGFade.GetComponent<Image> ().DOFade (1f, 2f);
		} 
		private void LogoAnim()
		{
			Quaternion qt = new Quaternion ();
			qt.eulerAngles = new Vector3 (0f, 90f, 0f);
			appTitle.localRotation = qt;

			appTitle.DOLocalRotate (new Vector3 (0f, 0f, 0f), 2.0f, RotateMode.Fast).OnComplete (() =>{

				Invoke ("LoadNextScene", loadDelay);
			});
		}
		private void LoadNextScene()
		{
			SceneManager.LoadScene (loadScene.ToString(), LoadSceneMode.Single);
		}
	}
		
}
