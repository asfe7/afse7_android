﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Afseh.HelperClasses;

namespace Afseh.Controllers 
{
	public interface IController {

		void LoadScene ();
		 
	}
}